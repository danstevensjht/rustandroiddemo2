package com.jht.rustandroiddemo2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    static {
        System.loadLibrary("rustandroiddemo2_rust_lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RustBindings g = new RustBindings();
        ((TextView)findViewById(R.id.hello_text_view)).setText(g.sayHello("furbies"));
    }
}