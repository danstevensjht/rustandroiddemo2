package com.jht.rustandroiddemo2;

// Based on https://krupitskas.github.io/posts/quest-dev-part-2/

public class RustBindings {
    private static native String greeting(final String pattern);

    public String sayHello(String to) {
        return greeting(to);
    }
}
