# Rust on Android proof-of-concept

Dan's Q4 smart goal:

> Write a "hello-world" application using the programming language Rust.
> 
> The goal is to experiment with multi-threading concepts.
> 
> Time box to 4 days.
> 
> Output is to show an application running and demonstrate the code to an alternative approach to the current way of object sharing.
